import unittest
from tkinter import *
import LF2_GUI
import math


class TestBeispieleTestCase(unittest.TestCase):


    def test_kilometersClick(self):

        def round_down(n, decimals=1):
            multiplier = 10 ** decimals
            return math.floor(n * multiplier) / multiplier
        global e
        root = Tk()
        e = Entry(root)
        e.pack()
        e.focus_set()
        my_string_var = StringVar()
        x = 0
        price = 1
        UserInput = e.get()

        while x < 4:
            price = price+0.5
            x = x+1
        
        my_string_var.set("You have to pay " + str(round_down(price)) + "€")

        self.assertEqual(str(round_down(price)),'3.0')





    def test_minutesClick(self):
    
        def round_down(n, decimals=1):
            multiplier = 10 ** decimals
            return math.floor(n * multiplier) / multiplier
        global e
        root = Tk()
        e = Entry(root)
        e.pack()
        e.focus_set()
        my_string_var = StringVar()
        x = 0
        price = 1
        UserInput = e.get()

        while x < 2:
            price = price+0.2
            x = x+1
        
        my_string_var.set("You have to pay " + str(round_down(price)) + "€")

        self.assertEqual(str(round_down(price)),'1.4')





if __name__=='__main__':
    unittest.main(verbosity=2)
