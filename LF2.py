import sys


def user_input():

    x = 0
    price = 1.00

    KMorMIN = input ("Do you want to calculate the charge via minutes or kilometers? (please use min or km):")


    if KMorMIN == "min":
        minutes = input ("How many minutes did you use the scooter for?:")
    elif KMorMIN == "km":
        kilometers = input ("How many kilometers did you use the scooter for?:")
    else:
        print("That is not a valid input. Please restart.")
        user_input()


    if KMorMIN == "min":
        while x < int(minutes):
            price = price+0.2
            x = x+1
        print("You have to pay " + str(price) + "€")

    if KMorMIN == "km":
        while x < int(kilometers):
            price = price+0.2
            x = x+1
        print("You have to pay " + str(price) + "€")

user_input()

