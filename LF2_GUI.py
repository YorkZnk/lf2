from tkinter import *
import math

root = Tk()

# Add image file
bg = PhotoImage(file = "e-scooter.png")
label1 = Label( root, image = bg)
label1.place(x = 0, y = -70)

my_string_var = StringVar()
my_string_var.set("Price:")

# Function for when the minutes button is pressed
def minutesClick():
    x = 0
    price = 1
    global e
    UserInput = e.get()

    while x < int(UserInput):
        price = price+0.2
        x = x+1
    
    my_string_var.set("You have to pay " + str(round_down(price)) + "€")
    
# Function for when the kilometers button is pressed
def kilometersClick():
    x = 0
    price = 1
    global label
    global e
    UserInput = e.get()

    while x < int(UserInput):
        price = price+0.5
        x = x+1
    
    my_string_var.set("You have to pay " + str(round_down(price)) + "€")

#function for rounding the price to 1 decimal
def round_down(n, decimals=1):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier) / multiplier

#creating the labels at the top of the screen
myLabel = Label(root, text="Price Calculator")
myLabel.pack()
myLabel = Label(root, text="Please input either your rented minutes or your driven kilometers and click the according button:")
myLabel.pack()

e = Entry(root)
e.pack()
e.focus_set()

#creating the buttons
b = Button(root, text="Minutes", command=minutesClick, fg="blue")
b.place(x=308, y=62)
c = Button(root, text="Kilometers", command=kilometersClick, fg="blue")
c.place(x=238, y=62)

#creating the display of the price at the bottom
my_label = Label(root, textvariable = my_string_var, font=("Arial", 25))
my_label.pack(side=BOTTOM)

#creating the exit button
button_quit = Button(root, text="Exit", command=root.quit)
button_quit.place(x=280, y=100)

#defining the size of the window
root.geometry("600x180")

root.mainloop()
